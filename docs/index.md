---
title: Home
tags:
  - email
  - synacor
---

## Ticketing
- eTicket for every Hosted email call.
- All email troubles are escalated Internally via eTicket once verified by a lead.
- Never escalate an email trouble to the member for further assistance unless otherwise stated in the process.


## Account Location

**NovaSubscriber**

- If unable to locate the customer’s email account via SPOG, search in NovaSubscriber under the Primary and any Secondary domains.

## Third-Party Equipment
- We fully support NRTC hosted email account setup, management, and troubleshooting.
- Troubleshooting is done by NRTC using remote support if allowed by the member.
- Never refer a customer to a third party for support! if unable to resolve the issue, escalate.

## Email Passwords
- We cannot retrieve passwords for customers. If the customer does not know their email password, we will need to change it. 
- Email passwords must be at least 8 characters and contain 3 out of 4 of the following: lowercase letter, uppercase letter, number, and special character. 
- Never put a customer's email password in a ticket. If requested to put the email password in the ticket by ops, always enter as an NNS Worklog!
- Password changes can take up to 15 minutes to provision. If the customer enters in an incorrect password five times in a row, their IP will be banned for 10 minutes.

 
 
## Storage Quota:
**10GB**
- Any mail in the Trash folder is moved to Recover Deleted Items after 7 days.
- Recover deleted items purges mail 30 days after it is added. Purged mail cannot be retrieved.

## Spam Folder
The Spam Folder is by default set to keep mail for 30 days before it is Purged.

Customers can whitelist trusted email addresses or domains to ensure mail is not sent to Spam.

## Restricted Attachments

!!! note "Note"
    
    **Attachments of the following type are restricted and cannot be sent:**

    <table>
    <tbody>
    <tr>
    <td><ul style="list-style-type:none;">
    <li>asd</li>
    <li>bat</li>
    <li>cab</li>
    <li>chm</li>
    <li>com</li>
    <li>cpl</li>
    <li>dll</li>
    <li>exe</li>
    <li>hlp</li>

    </ul></td>
    <td><ul style="list-style-type:none;">
    <li>hta</li>
    <li>inf</li>
    <li>lnk </li>
    <li>msi</li>
    <li>msp </li>
    <li>nws</li>
    <li>ocx</li>
    <li>pif</li>
    <li>reg </li></ul></td>
    <td><ul style="list-style-type:none;"><li>scr </li>
    <li>sct </li>
    <li>shb </li>
    <li>shs </li>
    <li>vbe </li>
    <li>vbs</li>
    <li>wsc</li>
    <li>wsf</li>
    <li>wsh</li></ul></td>
    </tr>
    </tbody>
    </table>