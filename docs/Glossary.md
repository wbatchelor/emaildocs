## Android

Linux-based operating system for mobile devices such as smartphones and tablets. Originally created by the Open Handset Alliance, it is now one of the largest smartphone platforms in the world.

***
## Application (App)

A computer software program designed to help the user perform specific tasks. Although most commonly applied as software for smartphones, apps also are used on other wired and wireless broadband networks.

***
## Attachment

A computer file that rides along with an email message. Attachments are widely used to transfer photos and documents to another party.

***
## BCC

BCC, or Blind Carbon Copy allows you to send copies of your email to multiple contacts without revealing the respective email addresses to your recipients.


***
## Bounceback


***
## Cache
## CC
## Cookie
## CPNI (Customer Proprietary ## Network Information)
## Distribution List
## Email Address
## Email Alias
## Email Body
## Email Conversion
## Email Group
## Email Header
## Email Migration
## Email Server
## Email Subject
## Firewall
## Full Realm
## Harvest
## IMAP
## iOS (iPhone OS)
## Linux OS
## Mac OS
## Mail App
## Mail Client
## Mail Filter
## Network
## Network Neutrality
## Number Portability
## ONT
## Operating System
## OS (Operating System)
## Outlook PST
## Phishing
## POP
## Port
## Security Certificate
## Smartphone
## SMTP
## Spam
## SPF (Sender Policy Framework)
## Spoofing
## SSL
## STARTTLS
## TLS
## Webmail
## Windows OS
