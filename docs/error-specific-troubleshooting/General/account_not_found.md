---
title: Account Not Found
---

| Ticket Requirements | Resources Required |
| ------------------ | ---------------- |
| Email Address: | NovaSubscriber | 


If you are unable to locate the account it is usually for one of the following reasons:

- Incorrect email address / domain.
- Account recently deactivated or purged by the Business Office.

### Process
1. Validate the customer is active in the member's account tool.
2. Search for the account using the full email address in the Quick Search located in Novasub under the Member's Primary and Secondary Domains.
    - This will account for any email aliases that the customer may be using.

3. Verify that the customer is using the correct email username and domain.
4. If you are still unable to locate the customer's email account, escalate.

### Escalation Requirements:
Ensure to input as much information as you can in the ticket , including when they last was able to access the account.
