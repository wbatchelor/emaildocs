---
title: Delayed Email
---

| Ticket Requirements | Resources Required |
| ------------------ | ---------------- |
| Email Address: | NovaSubscriber<br>[https://toolbox.googleapps.com/apps/messageheader/](https://toolbox.googleapps.com/apps/messageheader/) | 


If the customer is stating that a specific email was delayed or wishes to know more about where it came from, we can look at the headers to find out information. You may also sometimes need to send the headers to ops when you escalate a ticket.

### Process

#### Reading headers:
1. Navigate to the following website: https://toolbox.googleapps.com/apps/messageheader/
2. In a separate tab, Navigate to the customer’s webmail and locate the email in question.
3. Right-click the message and select show original
4. Copy the text on the page.
5. In "Paste email header here," paste your header.
6. Click Analyze the header above.

You can use this to tell where the email originated from, how long it took to get there (possible delays) see who the message was sent to

##### Example:
![](https://s3.amazonaws.com/zingtree-uploads/images%2F1627393747875-1627393747875.jpg)

*If the delay is prior to the email hitting our servers, then the delay is not on our end. If the delay is after it was received by our servers, escalate.*


### Escalation Requirements:
If an email is escalated to ops due to delays or questions pertaining to who it came from, gather the following:
- Email subject line
- Time/date email was received
- Add full email header to worklog of ticket
